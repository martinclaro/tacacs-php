# tacacs-php

TACACS+ Client developed in PHP.

This is a proof of concept.

## Supported Methods

- PAP Authentication

## TACACS+ Specification

For further information about TACACS+ please refer to: https://tools.ietf.org/html/draft-grant-tacacs-02

## Version

1.0.4

## Installation

```sh
$ git clone https://gitlab.com/martinclaro/tacacs-php.git tacacs-php
$ cd tacacs-php
$ composer update
$ php examples/tacacs-client.php
```
## Development

Want to contribute? Great!

## TODO's

- MS-CHAP Authentication

## License
GNU General Public License

**Free Software, Hell Yeah!**
